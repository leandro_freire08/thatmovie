import React, { Component } from 'react';
import {
  AppRegistry,
  Navigator,
  TouchableOpacity
} from 'react-native';

var SearchPage = require('./SearchPage');
var SearchResults = require('./SearchResults');
var MovieView = require('./MovieView');

class ThatMovie extends Component {

render() {
    return (
      <Navigator
          initialRoute={{id: 'SearchPage', name: 'Index'}}
          renderScene={this.renderScene.bind(this)}
          configureScene={(route) => {
            if (route.sceneConfig) {
              return route.sceneConfig;
            }
            return Navigator.SceneConfigs.FloatFromRight;
          }} />
    );
  }
  renderScene(route, navigator) {
    var routeId = route.id;
    if (routeId === 'SearchPage') {
      return (
        <SearchPage
          navigator={navigator} />
      );
    }

    if (routeId === 'SearchResults') {
      return (
        <SearchResults
          navigator={navigator}
          movies={route.movies}/>
      );
    }

     if (routeId === 'MovieView') {
      return (
        <MovieView
          navigator={navigator}
          movie={route.movie} />
      );
    }
    
    return this.noRoute(navigator);

  }
  noRoute(navigator) {
    return (
      <View style={{flex: 1, alignItems: 'stretch', justifyContent: 'center'}}>
        <TouchableOpacity style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}
            onPress={() => navigator.pop()}>
          <Text style={{color: 'red', fontWeight: 'bold'}}>An error occour, try again!</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

AppRegistry.registerComponent('ThatMovie', () => ThatMovie);
