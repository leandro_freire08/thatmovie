'use strict';
 
import React, { Component } from 'react';

import {
  WebView
} from 'react-native';

class MovieView extends Component {

  render() {
  var movie = this.props.movie;
    return (
       <WebView
        source={{uri: 'http://m.imdb.com/title/' + movie.imdb_id_long}}
        style={{marginTop: 20}}/>
    );
  }
}

module.exports = MovieView;
