'use strict';
 
import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  TouchableHighlight,
  TouchableOpacity,
  ActivityIndicator,
  Image,
  Navigator,
  ToastAndroid
} from 'react-native';
import SpeechAndroid from 'react-native-android-voice';

var SearchResults = require('./SearchResults');

var styles = StyleSheet.create({
  description: {
    marginBottom: 20,
    fontSize: 18,
    textAlign: 'center',
    color: '#656565'
  },
  container: {
    padding: 30,
    marginTop: 65,
    alignItems: 'center'
  },
  flowRight: {
	  flexDirection: 'row',
	  alignItems: 'center',
	  alignSelf: 'stretch'
	},
	buttonText: {
	  fontSize: 18,
	  color: 'white',
	  alignSelf: 'center'
	},
	button: {
	  height: 36,
	  flex: 1,
	  flexDirection: 'row',
	  backgroundColor: '#48BBEC',
	  borderColor: '#48BBEC',
	  borderWidth: 1,
	  borderRadius: 8,
	  marginBottom: 10,
	  alignSelf: 'stretch',
	  justifyContent: 'center'
	},
	searchInput: {
	  height: 36,
	  padding: 4,
	  marginRight: 5,
	  flex: 4,
	  fontSize: 18,
	  borderWidth: 1,
	  borderColor: '#48BBEC',
	  borderRadius: 8,
	  color: '#48BBEC'
	},
	image: {
		width: 200,
		height: 200
	}
});

function urlForQueryAndPage(key, value) {
  var data = {
  	  api_key: 'QaQ3J3DMSa3TFyfF'
  };
  data[key] = value.replace(/ /g, "+");
 
  var querystring = Object.keys(data)
    .map(key => key + '=' + encodeURIComponent(data[key]))
    .join('&');
 
  console.log(querystring);
  return 'https://api.whatismymovie.com/1.0/?' + querystring;
};

class SearchPage extends Component {
	constructor(props) {
	  super(props);
	  this.state = {
	    searchString: 'a boy learns he is a wizard',
	    isLoading: false,
	    message: ''
	  };
	}

	onSearchTextChanged(event) {
	  this.setState({ searchString: event.nativeEvent.text });
	}

	async _executeQuery(query) {
		this.setState({ isLoading: true });
	  	try {
		      let response = await fetch(query);
		      let responseJson = await response.json();
		      this._handleResponse(responseJson);
	    } catch(error) {
	     	this.setState({
		      isLoading: false,
		      message: 'Something bad happened ' + error
		   	});
	    }
	}

	_handleResponse(response) {
	  this.setState({ isLoading: false , message: '' });
	    this.props.navigator.push({
	      id: 'SearchResults',
	      sceneConfig: Navigator.SceneConfigs.FloatFromBottom,
			  title: 'Results',
			  movies: response
		});
	}

	async _buttonClick(){
	    try{
	        var spokenText = await SpeechAndroid.startSpeech("Speak yo", SpeechAndroid.US);
	        ToastAndroid.show(spokenText , ToastAndroid.LONG);
	        this.setState({ searchString: spokenText });
	        setTimeout(() => {this.onSearchPressed()}, 2000)
	    }catch(error){
	    	console.log(error);
	        switch(error){
	            case SpeechAndroid.E_VOICE_CANCELLED:
	                ToastAndroid.show("Voice Recognizer cancelled" , ToastAndroid.LONG);
	                break;
	            case SpeechAndroid.E_NO_MATCH:
	                ToastAndroid.show("No match for what you said" , ToastAndroid.LONG);
	                break;
	            case SpeechAndroid.E_SERVER_ERROR:
	                ToastAndroid.show("Google Server Error" , ToastAndroid.LONG);
	                break;
	        }
	    }
	}
	 
	onSearchPressed() {
	  var query = urlForQueryAndPage('text', this.state.searchString);
	  this._executeQuery(query);
	}

	render() {
  	var spinner = this.state.isLoading ?
	  ( <ActivityIndicator
	      size='large'/> ) :
	  ( <View/>);
    return (
      <View style={styles.container}>
        <Text style={styles.description}>
          Do you know?
          That movie...
        </Text>
        <Text style={styles.description}>
          Search movies by what you remember :)
        </Text>
        <View style={styles.flowRight}>
		  <TextInput
			  style={styles.searchInput}
			  value={this.state.searchString}
			  onChange={this.onSearchTextChanged.bind(this)}
			  placeholder='Anything you remember about the movie'/>
		  <TouchableHighlight style={styles.button}
		      underlayColor='#99d9f4'
		      onPress={this.onSearchPressed.bind(this)}
		      >
		  <Text style={styles.buttonText}>Go</Text>
		  </TouchableHighlight>
		</View>
        <Text style={styles.description}>
          Or tap image to voice search :D
        </Text>
	  	<TouchableOpacity onPress={this._buttonClick.bind(this)}>
			<Image source={require('./Resources/movie-icon.png')} style={styles.image}/>
		</TouchableOpacity>
		{spinner}
		<Text style={styles.description}>{this.state.message}</Text>
      </View>
    );
  }
}

module.exports = SearchPage;